#!/bin/bash

REGISTRY=your_registry_endpoint

docker load -i kube_1.9.1/kube-apiserver-amd64:v1.9.1.tar
docker load -i kube_1.9.1/kube-proxy-amd64:v1.9.1.tar
docker load -i kube_1.9.1/kube-controller-manager-amd64:v1.9.1.tar
docker load -i kube_1.9.1/kube-scheduler-amd64:v1.9.1.tar
docker load -i kube_1.8.3/kube-apiserver-amd64:v1.8.3.tar
docker load -i kube_1.8.3/kube-controller-manager-amd64:v1.8.3.tar
docker load -i kube_1.8.3/kube-scheduler-amd64:v1.8.3.tar
docker load -i kube_1.8.3/kube-proxy-amd64:v1.8.3.tar

docker tag gcr.io/google_containers/kube-apiserver-amd64:v1.9.1            $REGISTRY/kube-apiserver-amd64:v1.9.1          
docker tag gcr.io/google_containers/kube-proxy-amd64:v1.9.1                $REGISTRY/kube-proxy-amd64:v1.9.1              
docker tag gcr.io/google_containers/kube-controller-manager-amd64:v1.9.1   $REGISTRY/kube-controller-manager-amd64:v1.9.1 
docker tag gcr.io/google_containers/kube-scheduler-amd64:v1.9.1            $REGISTRY/kube-scheduler-amd64:v1.9.1          
docker tag gcr.io/google_containers/kube-apiserver-amd64:v1.8.3            $REGISTRY/kube-apiserver-amd64:v1.8.3          
docker tag gcr.io/google_containers/kube-controller-manager-amd64:v1.8.3   $REGISTRY/kube-controller-manager-amd64:v1.8.3 
docker tag gcr.io/google_containers/kube-scheduler-amd64:v1.8.3            $REGISTRY/kube-scheduler-amd64:v1.8.3          
docker tag gcr.io/google_containers/kube-proxy-amd64:v1.8.3                $REGISTRY/kube-proxy-amd64:v1.8.3              

docker push $REGISTRY/kube-apiserver-amd64:v1.9.1
docker push $REGISTRY/kube-proxy-amd64:v1.9.1    
docker push $REGISTRY/kube-controller-manager-amd64:v1.9.1
docker push $REGISTRY/kube-scheduler-amd64:v1.9.1
docker push $REGISTRY/kube-apiserver-amd64:v1.8.3
docker push $REGISTRY/kube-controller-manager-amd64:v1.8.3
docker push $REGISTRY/kube-scheduler-amd64:v1.8.3
docker push $REGISTRY/kube-proxy-amd64:v1.8.3    

